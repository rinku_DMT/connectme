//
//  AppDelegate.h
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/16/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


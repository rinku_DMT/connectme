//
//  SignUpViewController.m
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/17/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import "SignUpViewController.h"
CGFloat _currentKeyboardHeight = 0.0f;

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitButtonAction:(id)sender {
    
    //NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    //NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    //BOOL isValidPhoneNumber = [phoneTest evaluateWithObject:self.phoneNumberTextField.text];
    BOOL isValidPhoneNumber = YES;
    BOOL isValidEmailId = [self isValidEmail:self.emailIdTextField.text];
    BOOL isValidPassword = NO;
    BOOL isValidName = NO;
    BOOL isValidUserName = NO;
    NSString* alertMessage;
    
    if (self.passwordTextField.text.length >= 6 && self.passwordTextField.text.length < 14) {
        isValidPassword = YES;
    }
    else {
        alertMessage = @"Please enter valid password between 6 to 14 characters.";
    }
    if (self.nameTextField.text.length > 0) {
        isValidName = YES;
    }
    else {
        alertMessage = @"Please enter valid name";
    }
    if (self.userNameTextField.text.length > 4) {
        isValidUserName = YES;
    }
    else {
        alertMessage = @"Please enter valid userName of more than 4 characters.";
    }
    
    if (!isValidPhoneNumber) {
        alertMessage = @"Please enter valid phone number.";
    }
    if (!isValidEmailId) {
        alertMessage = @"Please enter valid email id.";
    }
    
    if (isValidEmailId && isValidName && isValidPassword && isValidUserName && isValidPhoneNumber) {
        NSDictionary* userData = [[NSDictionary alloc] initWithObjectsAndKeys:self.userNameTextField.text,@"username",self.passwordTextField.text,@"password",self.nameTextField.text,@"name",self.emailIdTextField.text,@"emailId",self.phoneNumberTextField.text,@"phoneNumber", nil];
        NSMutableArray* usersArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"usersArray"];
        if (usersArray && [usersArray count]) {
            [usersArray addObject:userData];
        }
        else {
            usersArray = [[NSMutableArray alloc] initWithObjects:userData, nil];
        }
        [[NSUserDefaults standardUserDefaults] setObject:usersArray forKey:@"usersArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        alertMessage = @"Account created successfully";
        
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:alertMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
    
}

-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


- (IBAction)clearButtonAction:(id)sender {
    self.nameTextField.text = @"";
    self.userNameTextField.text = @"";
    self.passwordTextField.text = @"";
    self.phoneNumberTextField.text = @"";
    self.emailIdTextField.text = @"";
}

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat deltaHeight = kbSize.height - _currentKeyboardHeight;
    // Write code to adjust views accordingly using deltaHeight
    _currentKeyboardHeight = kbSize.height;
}

- (void)keyboardWillHide:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    // Write code to adjust views accordingly using kbSize.height
    _currentKeyboardHeight = 0.0f;
    self.scrollView.contentOffset = CGPointMake(0,0);
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height);
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.tag == 104) {
        self.scrollView.contentOffset = CGPointMake(0,150);
    }
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height+150);
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

@end

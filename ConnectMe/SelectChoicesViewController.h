//
//  SelectChoicesViewController.h
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/18/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreferencesCutomCell.h"

@interface SelectChoicesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet PreferencesCutomCell *preferencesCutomCell;

- (IBAction)saveChoicesButtonAction:(id)sender;
@end

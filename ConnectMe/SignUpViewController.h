//
//  SignUpViewController.h
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/17/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailIdTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)submitButtonAction:(id)sender;
- (IBAction)clearButtonAction:(id)sender;

@end

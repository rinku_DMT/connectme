//
//  SelectChoicesViewController.m
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/18/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import "SelectChoicesViewController.h"
#import "PreferencesCutomCell.h"
#import "DataManager.h"

@interface SelectChoicesViewController () {
    NSArray* foodPreferencesArray;
    NSArray* clothingPreferencesArray;
}

@end

@implementation SelectChoicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    foodPreferencesArray = [[NSArray alloc] initWithObjects:@"Dominos",@"Barbeque Nation",@"Yo China!!!", nil];
    clothingPreferencesArray = [[NSArray alloc] initWithObjects:@"Peter England",@"Louis Philippe",@"Arrow", nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"preferencesCutomCell";
    
    PreferencesCutomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = self.preferencesCutomCell;
    }
    
    UserDataModel* user = [[DataManager sharedManager] loggedUser];
    NSArray* foodPreferences = user.foodPreferencesArray;
    NSArray* clothingPreferences = user.clothingPreferencesArray;
    
    NSInteger section = [indexPath section];
    
    switch (section) {
        case 0: // First cell in section 1
        {
            cell.textLabel.text = [foodPreferencesArray objectAtIndex:indexPath.row];
            for (int i = 0; i < foodPreferences.count; ++i) {
                if ([cell.textLabel.text isEqualToString:[foodPreferences objectAtIndex:i]]) {
                    [cell setCheckBoxSelectedForPreference:[foodPreferences objectAtIndex:i]];
                }
            }
        }
            break;
        case 1: // Second cell in section 1
        {
            cell.textLabel.text = [clothingPreferencesArray objectAtIndex:indexPath.row];
            for (int i = 0; i < clothingPreferences.count; ++i) {
                if ([cell.textLabel.text isEqualToString:[clothingPreferences objectAtIndex:i]]) {
                    [cell setCheckBoxSelectedForPreference:[clothingPreferences objectAtIndex:i]];
                }
            }
        }
            break;
        case 2: // Third cell in section 1
            cell.textLabel.text = [NSString stringWithFormat:@"Food Item %d",indexPath.row+1];
            break;
        case 3: // Fourth cell in section 1
            cell.textLabel.text = [NSString stringWithFormat:@"Food Item %d",indexPath.row+1];
            break;
        default:
            // Do something else here if a cell other than 1,2,3 or 4 is requested
            break;
    }
    //cell.checkBoxSelected = YES;
    cell.backgroundColor = [UIColor whiteColor];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Uncheck the previous checked row
//    if(self.checkedIndexPath)
//    {
//        UITableViewCell* uncheckCell = [tableView
//                                        cellForRowAtIndexPath:self.checkedIndexPath];
//        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
//    }
//    if([self.checkedIndexPath isEqual:indexPath])
//    {
//        self.checkedIndexPath = nil;
//    }
//    else
//    {
//        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        self.checkedIndexPath = indexPath;
//    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Food Items";
    }
    else if(section == 1) {
        return @"Clothing";
    }
    return nil;
}

- (IBAction)saveChoicesButtonAction:(id)sender {
    NSMutableArray *foodSectionPreferences = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < [self.tableView numberOfRowsInSection:0]; ++i)
        {
            PreferencesCutomCell* cell = (PreferencesCutomCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            if (cell.checkBoxSelected) {
                [foodSectionPreferences addObject:cell.textLabel.text];
            }
        }
    NSMutableArray *clothingSectionPreferences = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < [self.tableView numberOfRowsInSection:1]; ++i)
    {
        PreferencesCutomCell* cell = (PreferencesCutomCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:1]];
        if (cell.checkBoxSelected) {
            [clothingSectionPreferences addObject:cell.textLabel.text];
        }
    }
    UserDataModel* user = [[DataManager sharedManager] loggedUser];
    user.foodPreferencesArray = [[NSMutableArray alloc] initWithArray:foodSectionPreferences];
    user.clothingPreferencesArray = [[NSMutableArray alloc] initWithArray:clothingSectionPreferences];
    
    NSMutableArray* usersArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"usersArray"]];
    for (int i = 0; i < usersArray.count; ++i)
    {
        if ([user.userName isEqualToString:[[usersArray objectAtIndex:i] objectForKey:@"username"]] &&
            [user.password isEqualToString:[[usersArray objectAtIndex:i] objectForKey:@"password"]]) {
         NSDictionary* userData = [[NSDictionary alloc] initWithObjectsAndKeys: user.userName,@"username",user.password,@"password",user.name,@"name",user.emailId,@"emailId",user.phoneNumber,@"phoneNumber",foodSectionPreferences,@"foodSectionPreferences",clothingSectionPreferences,@"clothingSectionPreferences", nil];
            [usersArray replaceObjectAtIndex:i withObject:userData];
            [[NSUserDefaults standardUserDefaults] setObject:usersArray forKey:@"usersArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"CurrentOffersViewController"];
            //vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:vc animated:YES completion:NULL];
            return;
        }
        
    }
}
@end

//
//  ViewController.h
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/16/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)loginButtonAction:(id)sender;

@end


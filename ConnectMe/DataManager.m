//
//  DataManager.m
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/19/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

+ (id)sharedManager {
    static DataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
@end

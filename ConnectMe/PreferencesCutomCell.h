//
//  PreferencesCutomCell.h
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/19/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreferencesCutomCell : UITableViewCell

@property(nonatomic, assign) BOOL checkBoxSelected;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton;

- (IBAction)checkBoxButtonClicked:(id)sender;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
-(void) setCheckBoxSelectedForPreference:(NSString *) preference;

@end

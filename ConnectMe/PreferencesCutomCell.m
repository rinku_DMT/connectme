//
//  PreferencesCutomCell.m
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/19/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import "PreferencesCutomCell.h"

@implementation PreferencesCutomCell

- (IBAction)checkBoxButtonClicked:(id)sender {
    if(self.checkBoxSelected) {
        self.checkBoxSelected = NO;
        [self.checkBoxButton setImage:[UIImage imageNamed:@"empty_checkbox.png"] forState:UIControlStateNormal];
    }
    else {
        self.checkBoxSelected = YES;
        [self.checkBoxButton setImage:[UIImage imageNamed:@"selected_checkBox.png"] forState:UIControlStateNormal];
    }

}

-(void) setCheckBoxSelectedForPreference:(NSString *) preference{
    if ([self.textLabel.text isEqualToString:preference]) {
        self.checkBoxSelected = YES;
        [self.checkBoxButton setImage:[UIImage imageNamed:@"selected_checkBox.png"] forState:UIControlStateNormal];
    }
    else {
        self.checkBoxSelected = NO;
        [self.checkBoxButton setImage:[UIImage imageNamed:@"empty_checkbox.png"] forState:UIControlStateNormal];
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.factory = [GLLayoutFactory factory];
//        self.isCheckboxSelected = NO;
//        self.currentMode = NORMAL_MODE;
    }
    return self;
}

@end

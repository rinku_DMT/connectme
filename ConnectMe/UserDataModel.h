//
//  UserDataModel.h
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/19/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataModel : NSObject

@property(nonatomic, strong) NSString* userName;
@property(nonatomic, strong) NSString* password;
@property(nonatomic, strong) NSString* name;
@property(nonatomic, strong) NSString* emailId;
@property(nonatomic, strong) NSString* phoneNumber;
@property(nonatomic, strong) NSMutableArray* foodPreferencesArray;
@property(nonatomic, strong) NSMutableArray* clothingPreferencesArray;

- (instancetype)initWithDictionary:(NSDictionary *)parameterDictionary;
@end

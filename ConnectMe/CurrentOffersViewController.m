//
//  CurrentOffersViewController.m
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/20/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import "CurrentOffersViewController.h"
#import "DataManager.h"
#import "AppDelegate.h"

@interface CurrentOffersViewController () 
@property (nonatomic, strong) ABBeaconRegion *region;

@end

@implementation CurrentOffersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.beaconManager = [[ABBeaconManager alloc] init];
    self.beaconManager.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)beaconManager:(ABBeaconManager *)manager didEnterRegion:(ABBeaconRegion *)region
{
    UserDataModel* user = [[DataManager sharedManager] loggedUser];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = [NSString stringWithFormat:@"Hello %@!!! Welcome to shopping zone.",user.name];
    notification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

- (void)beaconManager:(ABBeaconManager *)manager didExitRegion:(ABBeaconRegion *)region
{
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    UserDataModel* user = [[DataManager sharedManager] loggedUser];
    notification.alertBody = [NSString stringWithFormat:@"Bye %@!!! Have a nice day.",user.name];
    notification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

- (void)startMonitoringForRegion
{
    if (!_region) {
        self.region = [[ABBeaconRegion alloc] initWithProximityUUID:_beacon.proximityUUID identifier:_beacon.proximityUUID.UUIDString];
    } else {
        [_beaconManager stopMonitoringForRegion:self.region];
    }
    self.region.notifyOnEntry = YES;
    self.region.notifyOnExit = YES;
    self.region.notifyEntryStateOnDisplay = YES;
    [_beaconManager startMonitoringForRegion:self.region];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

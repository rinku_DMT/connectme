//
//  ViewController.m
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/16/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import "ViewController.h"
#import "DataManager.h"
#import "UserDataModel.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonAction:(id)sender {
    if (self.userNameTextField.text.length && self.passwordTextField.text.length) {
        NSArray* usersArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"usersArray"];
        for (int i = 0; i < usersArray.count; ++i)
        {
            if ([self.userNameTextField.text isEqualToString:[[usersArray objectAtIndex:i] objectForKey:@"username"]] &&
                [self.passwordTextField.text isEqualToString:[[usersArray objectAtIndex:i] objectForKey:@"password"]]) {
                UserDataModel* user = [[UserDataModel alloc] initWithDictionary:[usersArray objectAtIndex:i]];
                if ([[usersArray objectAtIndex:i] objectForKey:@"foodSectionPreferences"]) {
                    user.foodPreferencesArray = [[usersArray objectAtIndex:i] objectForKey:@"foodSectionPreferences"];
                }
                if ([[usersArray objectAtIndex:i] objectForKey:@"clothingSectionPreferences"]) {
                    user.clothingPreferencesArray = [[usersArray objectAtIndex:i] objectForKey:@"clothingSectionPreferences"];
                }
                [[DataManager sharedManager] setLoggedUser:user];
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SelectChoicesViewController"];
                //vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self presentViewController:vc animated:YES completion:NULL];
                return;
            }
        }
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Try Again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

@end

//
//  DataManager.h
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/19/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDataModel.h"

@interface DataManager : NSObject

+ (id)sharedManager;

@property(nonatomic, strong) UserDataModel* loggedUser;


@end

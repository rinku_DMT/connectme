//
//  CurrentOffersViewController.h
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/20/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AprilBeaconSDK.h"

@interface CurrentOffersViewController : UIViewController<ABBeaconManagerDelegate>

@property (nonatomic, strong) ABBeaconManager *beaconManager;
@property (nonatomic, strong) ABBeacon *beacon;

@end

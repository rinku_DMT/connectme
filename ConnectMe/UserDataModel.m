//
//  UserDataModel.m
//  ConnectMe
//
//  Created by Rinku Dahiya on 3/19/16.
//  Copyright (c) 2016 Rinku Dahiya. All rights reserved.
//

#import "UserDataModel.h"

@implementation UserDataModel

- (instancetype)initWithDictionary:(NSDictionary *)parameterDictionary
{
    if(self = [super init])
    {
        self.userName = [parameterDictionary objectForKey:@"username"];
        self.password = [parameterDictionary objectForKey:@"password"];
        self.name = [parameterDictionary objectForKey:@"name"];
        self.emailId = [parameterDictionary objectForKey:@"emailId"];
        self.phoneNumber = [parameterDictionary objectForKey:@"phoneNumber"];
    }
    return self;
}

@end
